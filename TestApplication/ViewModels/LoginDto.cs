﻿using System.ComponentModel.DataAnnotations;

namespace TestApplication.Models;

public class LoginDto
{
    [Required(ErrorMessage = "Логин обязателен")]
    public required string Login { get; set; }

    [Required(ErrorMessage = "Пароль обязателен")]
    public required string Password { get; set; }
}