﻿namespace TestApplication.Models;

public class MyTasksDto
{
   public string Name { get; set; }
   public string Surname { get; set; }
   public List<TaskModel> Tasks { get; set; }
}