﻿namespace TestApplication.Models;

public class TaskDto
{
    public long Id { get; set; }
    public required string PhoneNumber { get; set; }
    public required string Name { get; set; }
    public required string Surname { get; set; }
    public required string Email { get; set; }
    
    public string Type { get; set; }
}