﻿using System.ComponentModel.DataAnnotations;

namespace TestApplication.Models;

public class UserDto
{
    [Required(ErrorMessage = "Логин обязателен")]
    public required string Login { get; set; }

    [Required(ErrorMessage = "Имя обязательно")]
    public required string Name { get; set; }

    [Required(ErrorMessage = "Фамилия обязательна")]
    public required string Surname { get; set; }

    [Required(ErrorMessage = "Email обязателен")]
    [EmailAddress(ErrorMessage = "Некорректный формат Email")]
    public required string Email { get; set; }

    [Required(ErrorMessage = "Пароль обязателен")]
    public required string Password { get; set; }

    [Required(ErrorMessage = "Подтверждение пароля обязательно")]
    [Compare("Password", ErrorMessage = "Пароли не совпадают")]
    public required string ConfirmPassword { get; set; }
    
}