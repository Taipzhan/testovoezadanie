﻿namespace TestApplication.Models;

public class TypeTask
{
    public long Id { get; set; }
    public required string Name { get; set; }
}