﻿using Microsoft.AspNetCore.Identity;

namespace TestApplication.Models;

public class User : IdentityUser
{
    public required string Login { get; set; }
    public required string Name { get; set; }
    public required string Surname { get; set; }
}