﻿namespace TestApplication.Models;

public class Statistic
{
    public long Id { get; set; }
    
    public string UserId { get; set; }
    public User User { get; set; }
    
    public long TaskId { get; set; }
    public TaskModel Task { get; set; }
}