﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TestApplication.Models;

namespace TestApplication.DataBase;

public class TaskDbContext : IdentityDbContext<User>
{
    public required DbSet<TaskModel> TaskModels { get; set; }
    public required DbSet<TypeTask> TypeTasks { get; set; }
    public required DbSet<User> Users { get; set; }
    public required DbSet<Statistic> Statistics { get; set; }

    public TaskDbContext(DbContextOptions<TaskDbContext> options) : base(options)
    {
        
    }
}