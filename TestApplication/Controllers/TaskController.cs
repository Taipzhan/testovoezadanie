﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestApplication.DataBase;
using TestApplication.Models;

namespace TestApplication.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TaskController: ControllerBase
{
    private readonly TaskDbContext _db;
    private readonly UserManager<User> _userManager;
    
    public TaskController(TaskDbContext db, UserManager<User> userManager)
    {
        _db = db;
        _userManager = userManager;
    }
    

    [HttpGet]
    public IActionResult Get()
    {
        return Ok(_db.TaskModels.ToList());
    }
    
    [HttpGet("{id:int}")]
    public IActionResult Get(int Id)
    {
        var task = _db.TaskModels.FirstOrDefault(t => t.Id == Id);
        if (task != null)
        {
            return NotFound("Заявка не найдена");
        }

        return Ok(task);
    }

    [HttpPost]
    public async Task<ActionResult<TaskModel>> Post(TaskModel? taskModel)
    {
        if (taskModel is null)
        {
            return BadRequest();
        }

        var tasktype = _db.TypeTasks.FirstOrDefault(t=>t.Id == taskModel.TaskTypeId);
        if(tasktype == null)
        {
            return NotFound("Такого типа заявки нет");
        }
        await _db.TaskModels.AddAsync(taskModel);
        await _db.SaveChangesAsync();
        return Ok(taskModel);
    }
    
}