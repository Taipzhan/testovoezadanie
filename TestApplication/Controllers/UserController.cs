﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestApplication.DataBase;
using TestApplication.Models;

namespace TestApplication.Controllers;

[ApiController]
[Route("api/[controller]")]
public class UserController : ControllerBase
{
    private readonly TaskDbContext _db;
    private readonly UserManager<User> _userManager;
    private readonly SignInManager<User> _signInManager;
    
    public UserController(TaskDbContext db, UserManager<User> userManager, SignInManager<User> signInManager)
    {
        _db = db;
        _userManager = userManager;
        _signInManager = signInManager;
    }
    
    
    [HttpPost("Register")]
    public async Task<IActionResult> Post(UserDto? vm)
    {
        if (vm != null && ModelState.IsValid)
        {
            var user = new User{ UserName = vm.Login,Email = vm.Email, Name = vm.Name, Surname = vm.Surname, Login = vm.Login};

            var result = await _userManager.CreateAsync(user, vm.Password);
            if (result.Succeeded)
            {
                await _signInManager.SignInAsync(user, false);
                return Ok("Registration complete");
            }
            foreach (var identityError in result.Errors)
            {
                ModelState.AddModelError(string.Empty, identityError.Description);
            }
        }
        ModelState.AddModelError(String.Empty,"Не все поля правильно заполнены");
        return BadRequest(ModelState);
    }
    
    
    [HttpPost("Login")]
    public async Task<IActionResult> Post(LoginDto? vm)
    {
        if(ModelState.IsValid)
        {
            var user =  _db.Users.FirstOrDefault(u => u.Login == vm.Login);
            if (user is not null)
            {
                var result = await _signInManager.PasswordSignInAsync(user, vm.Password, true, false);
                if (result.Succeeded)
                {
                    return Ok("Усешный вход");
                }
                else
                {
                    return BadRequest("Неверный логин или пароль");
                }
            }
            else
            {
                return BadRequest("Пользователь не найден");   
            }
        }
        else
        {
            return BadRequest("Не все поля заполнены");
        }

        return BadRequest(ModelState);
    }
    

    [Authorize]
    [HttpGet("Logout")]
    public async Task<IActionResult> Get()
    {
        await _signInManager.SignOutAsync();
        return Ok("Успешный выход из системы");
    }


    [Authorize]
    [HttpGet("MyTasks")]
    public async Task<IActionResult>GetTask()
    {
        var user = await _userManager.GetUserAsync(User);
        var tasks = await _db.Statistics.Where(t => t.UserId == user.Id).Include(t => t.Task)
            .Select(t=>t.Task)
            .ToListAsync();
        var mytasks = new MyTasksDto {Name = user.Name, Surname = user.Surname, Tasks = tasks};
        return Ok(mytasks);
;    }
    
    [Authorize]
    [HttpPost("GetTask")]
    public async Task<ActionResult> Post(long id)
    {
        var task = _db.TaskModels.FirstOrDefault(t => t.Id == id);
        if (task == null)
        {
            return BadRequest("Заявка не найдена");
        }
    
        var user = await _userManager.GetUserAsync(User);
        var data = new Statistic{UserId = user.Id, TaskId = task.Id};
        _db.Statistics.Add(data);
        _db.SaveChanges();
        return Ok("Заявка взята в работу");
    }
}