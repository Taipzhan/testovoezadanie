﻿using Microsoft.AspNetCore.Mvc;
using TestApplication.DataBase;
using TestApplication.Models;

namespace TestApplication.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TypeTaskController : ControllerBase
{
    private readonly TaskDbContext _db;
    public TypeTaskController(TaskDbContext db)
    {
        _db = db;
    }

    [HttpGet]
    public ActionResult Get()
    {
        return Ok(_db.TypeTasks.ToList());
    }

    
    [HttpDelete("id: int")]
    public ActionResult Delete(int? id)
    {
        var task = _db.TypeTasks.FirstOrDefault(t => t.Id == id);
        if (task == null)
        {
            return NotFound();
        }

        _db.TypeTasks.Remove(task);
        _db.SaveChanges();
        return Ok("Заявка удалена");
    }

    [HttpPost]
    public async Task<ActionResult<TypeTask>> Post(TypeTask? typeTask)
    {
        if (typeTask == null)
        {
            return BadRequest();
        }

        await _db.TypeTasks.AddAsync(typeTask);
        await _db.SaveChangesAsync();
        return Ok(typeTask);
    }
}